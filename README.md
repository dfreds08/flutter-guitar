# flutter_guitar

A simple guitar chord project built with Flutter in under 5 KB.

Please note that an iOS emulator will not play the sounds.

I also modified my formatter to wrap lines at 120 instead of 80. This saved a lot of useless bytes around whitespacing while still maintaining code quality.

## Checking Size

Run this command: `find . -name "*.dart" | xargs cat | wc -c`

## References

40 = open 6 E

45 = open 5

50 = open 4

55 = open 3

59 = open 2

64 = open 1 E

https://flutter.dev/create

https://rodydavisjr.com/2019/03/12/making-a-piano/
