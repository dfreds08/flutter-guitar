import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_midi/flutter_midi.dart';
import 'package:flutter/services.dart';

final brown = Color(0xff51373b);
final orange = Color(0xffea573b);
final red = Color(0xffd43f45);
final white = Colors.white;

main() => runApp(App());

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  Chord chord;
  List<Chord> chords = [];

  _loadJson() async {
    var chordsJson = await rootBundle.loadString("assets/chords.json");

    setState(() {
      chords = (json.decode(chordsJson) as List).map((c) => Chord.fromJson(c)).toList();
      chord = chords[0];
    });
  }

  int _calcMidi(int string, {int fret = 0}) => (string < 3 ? 64 : 65) - ((string - 1) * 5) + fret;

  _strum() {
    var midis = chord.positions.map((p) => _calcMidi(p.string, fret: p.fret)).toList()
      ..addAll(chord.open.map((o) => _calcMidi(o)));

    midis.forEach((i) => FlutterMidi.playMidiNote(midi: i));
  }

  @override
  initState() {
    _loadJson();
    FlutterMidi.unmute();
    rootBundle.load("assets/guitar.sf2").then((sf2) {
      FlutterMidi.prepare(sf2: sf2, name: "guitar.sf2");
    });
    super.initState();
  }

  @override
  Widget build(context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Guitar Chords")),
        body: Padding(child: _buildBody(), padding: EdgeInsets.all(8)),
        bottomNavigationBar: _BottomAppBar(builder: (c) => _buildDrawer(c), name: chord?.name ?? ""),
        floatingActionButton: FloatingActionButton(child: Icon(Icons.music_note), onPressed: () => _strum()),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      ),
      theme: ThemeData(
        accentColor: orange,
        bottomAppBarColor: red,
        brightness: Brightness.dark,
        canvasColor: brown,
        primaryColor: red,
      ),
      title: "Guitar Chords",
    );
  }

  Widget _buildBody() {
    if (chord == null) return Center(child: CircularProgressIndicator());

    return GridView.count(
      crossAxisCount: 6,
      children: List.generate(30, (i) => FretString(chord: chord, fret: i ~/ 6 + 1, string: 6 - (i % 6))),
    );
  }

  Widget _buildDrawer(context) {
    var children = <Widget>[];

    children.addAll(chords
        .map((c) => ListTile(
            onTap: () {
              Navigator.of(context).pop();
              setState(() => chord = c);
            },
            selected: chord == c,
            title: Text(c.name)))
        .toList());

    return Drawer(child: ListView(children: children));
  }
}

class _BottomAppBar extends StatelessWidget {
  _BottomAppBar({this.builder, this.name});

  final WidgetBuilder builder;
  final String name;

  @override
  Widget build(context) {
    return BottomAppBar(
      child: Row(
        children: [
          IconButton(
            icon: Icon(Icons.menu, color: white),
            onPressed: () => showModalBottomSheet(context: context, builder: builder),
          ),
          SizedBox(width: 4),
          Text(name, style: TextStyle(color: white, fontSize: 18, fontWeight: FontWeight.bold)),
        ],
      ),
      shape: CircularNotchedRectangle(),
    );
  }
}

class FretString extends StatelessWidget {
  FretString({this.chord, this.fret, this.string});

  final Chord chord;
  final int fret;
  final int string;

  Widget _buildText(String s) {
    var style = TextStyle(fontSize: 28, color: white, fontWeight: FontWeight.bold);
    return Positioned(child: Text(s, style: style), top: 0);
  }

  @override
  Widget build(context) {
    var decor = BoxDecoration(border: Border.all(color: white));
    var c = <Widget>[Container(decoration: decor, width: 2)];

    if (fret == 1 && chord.muted.contains(string)) c.add(_buildText("X"));
    if (fret == 1 && chord.open.contains(string)) c.add(_buildText("O"));

    var p = chord.positions.firstWhere((p) => p.string == string && p.fret == fret, orElse: () => null);

    if (p != null) c.add(CircleAvatar(foregroundColor: white, backgroundColor: orange, child: Text("${p.finger}")));

    if (fret != 5) c.add(Positioned(bottom: 0, left: 0, right: 0, child: Container(decoration: decor, height: 1)));

    return Stack(alignment: Alignment.center, children: c);
  }
}

class Chord {
  Chord({this.muted, this.name, this.open, this.positions});

  factory Chord.fromJson(json) => Chord(
        muted: (json["muted"] as List).map((s) => int.parse(s.toString())).toList(),
        name: json["name"],
        open: (json["open"] as List).map((s) => int.parse(s.toString())).toList(),
        positions: (json["positions"] as List).map((p) => Position.fromJson(p)).toList(),
      );

  final List<int> muted;
  final String name;
  final List<int> open;
  final List<Position> positions;
}

class Position {
  Position({this.fret, this.finger, this.string});

  factory Position.fromJson(json) => Position(finger: json["finger"], fret: json["fret"], string: json["string"]);

  final int finger;
  final int fret;
  final int string;
}
